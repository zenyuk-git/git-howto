diff between the commit and it's parent(previous) only
==
git diff 1a2b3c^!


show commits difference between two brances, commits missing existing in one, but missing in another
==
git log --abbrev-commit "MISSING_IN_THIS_BRANCH".."YOUR_NEWER_BRANCH" --oneline --no-merges --after="2015-06-30 16:24:00"


show branches tree
==
git log --graph --oneline --all


show branches in order they were created (NOT ancestors)
==
git show-branch


git log short format, in one line
==
git log --oneline


create a patch file from a diff
==
git diff > patch_file.diff


restore/apply git patch file
==
git apply path_to_patch_file


see changes/diff of a added/staged file
==
git diff --staged path/to/file


gitlab/github is asking for credentials, but SSH key was copied. E.g. after go get bla
==
update ~/.gitconfig with:
[url "ssh://git@gitlab.com/"]
    insteadOf = https://gitlab.com/

or individually:
navigate to the root of the checked out project
go to .git folder
open "config" file
replace http/https with ssh and add user "git@" like:
[remote "origin"]
	url = ssh://git@gitlab.com/zenyukgo/go_basics.git


test ssh connection to git server
==
ssh -T git@gitlab.com


empty commit
==
git commit --allow-empty -m "a message"


list tags
==
git tag


create annotated tag
==
git tag -a v1.4 -m "my version 1.4"


show tag details
==
git show v1.4


compare branches
==
git diff branch1..branch2


squash WITHOUT force and rebase
==
git checkout YOUR_RELEASE_BRANCH
git pull
git checkout -b A_NEW_BRANCH
git merge --squash YOUR_BRANCH_WITH_MULTIPLE_COMMITS
git commit -am "squashing all commits into one"
git push --set-upstream origin A_NEW_BRANCH


squash multiple commits in a feature branch (with rebase and force)
==
without creating extra branch:
$ git log - see someone else’s commit just before your first one 
$ git rebase -i SOMEONE_ELSES_COMMIT
a list will be given in vi editor, edit it:
leave the top line with “pick”  command
in all subsequent lines replace “pick” with “squash”
save & close (esc, :wq)
another vi editor will show up; edit message (delete extra noise line)
$ git status  
will show diverged state - expected
$ git push -f 
(force push)


git clone error
unable to access ...git SSL certificate problem: self signed certificate in certificate chain
==
git config --global http.sslVerify false


git warning: CRLF will be replaced by LF in
to hide it:
==
git config --global core.autocrlf true


lf vs crlf - make git unaware
==
git config --global --unset core.autocrlf


Generate key
==
ssh-keygen -t rsa
public key: cat ~/.ssh/id_rsa.pub


show not pushed commits
==
git log origin/master..master


Fix git errors
==
git reset --hard
git clean -df


Revert local commits
==
git reset --hard origin/master (or origin/develop)


Revert all local unsaved changes
==
git clean -df 
git checkout -- .


Stash local changes
==
git stash
or
git stash save --keep-index


Clean stash
==
git stash drop


show stashes
==
git stash list 
git stash show [<stash>]


Diff against last stash
==
git stash show -p stash@{0}


stash with a message
==
git stash push -m "YOUR_MESSAGE"


Revert a single file to a revision
==
git checkout abcde file/to/restore   (Assuming the commit you want is abcde)


Check pending changes
==
git diff


Add local changes to commit
==
git add *.java


Revert changes to a file
==
git checkout /path/to/file.txt


Ignore files locally (not versioned files)
==
Add files or folders one per line to

* Globally for all repos:
$HOME/.config/git/ignore

* Individually per repo:
.git/info/exclude


Ignore files locally (versioned files)
==
git update-index --assume-unchanged [<file>...]


Last commits of a user
==
git log --pretty=format:"%h - %an, %ar” | grep <USER_NAME>


List all available branches
==
git branch -a


Switch branch to a branch called “mybranch1”
==
git branch mybranch1
git checkout mybranch1
git branch -u origin/mybranch1 mybranch1
git pull --rebase ??


create branch and switch to it
==
git checkout -b YOUR_BRANCH


push to new brach
==
git push -u origin YOUR_BRANCH


show current brach (or show all local branches)
==
git branch 
// current will be marked with *


show changes in commit
==
git show COMMIT_ID


change user name
==
git config --global user.name "Billy Gee"


show repo name
==
git config --get remote.origin.url


resolve merge conflicts (in gitflow with pull requests)
==
1st option with merge (NOT rebase):
while in a feature branch
$ git pull origin develop
RESOLVE CONFLICTS MANUALLY
$ git commit -m “resolve merge conflicts”
$ git push

2 option with rebase:
while in a feature branch
$ git checkout develop
$ git pull
$ git checkout <YOUR_TICKET>
$ git rebase develop
RESOLVE CONFLICTS MANUALLY
$ git rebase —continue (—skip)
$ git push —force


revert commit in master
==
git checkout master
git pull
git log //to see revision you want to revert
git revert <YOUR_REVISION>
git push


merge develop into master in gitflow
==
git checkout develop
git pull
git checkout master
git pull
git merge develop --ff --no-edit
git push


unversion folder and delete from disk
==
git rm -r <YOUR_FOLDER>


unversion folder but DO NOT delete from disk
==
git rm --cached -r <YOUR_FOLDER>


git cherry pick a commit (from local branch)
==
checkout the target branch, then
git cherry-pick <COMMIT_REV>
open & edit conflict files (search by ====)
git add .

// if you need to continue after the conflict resolution, otherwise you are done
git cherry-pick --continue


changes in a commit
==
git diff <COMMIT_REV>^ <COMMIT_REV>
e.g.: git diff 3b45f^ 3b45f


show only filenames in a commit
==
git diff 3b45f^ 3b45f --name-only


return back to a branch from a detached head
==
git checkout <YOUR_BRANCH>


undo “git add” for a file
==
git reset <YOUR_FILE>
or for all files (git add .): “git reset”


change commit message
==
git commit --amend
and edit message, exit with “wq”


change author of the last local commit (authorization issue)
==
git commit --amend --author="Author Name <email@address.com>"

or hard way (tested):
For example, if your commit history is A-B-C-D-E-F with F as HEAD, and you want to change the author of C and D, then you would...
Specify git rebase -i B 
if you need to edit A, use git rebase -i --root
change the lines for both C and D from pick to edit
Once the rebase started, it would first pause at C
You would git commit --amend --author="Author Name <email@address.com>"
Then git rebase --continue
It would pause again at D
Then you would git commit --amend --author="Author Name <email@address.com>" again
git rebase --continue
The rebase would complete.


unstage (remove from changed list before commit)
==
git reset <YOUR_FILE>


restore deleted file
==
find the last commit that affected the given path
git rev-list -n 1 HEAD -- <file_path>

then checkout the version at the commit before, using the caret (^) symbol:
git checkout <deleting_commit>^ -- <file_path>


see commits by a path
==
git log -- YOUR_PATH_HERE


log format
==
git log --pretty=format:"%h by %cn on %cd"


see pending commits
==
git diff --stat --cached origin/master


difference with prev revision
==
$ git diff HEAD^
or for a file
$ git diff HEAD^ -- /path/to/file.txt


diff for a file against another branch and number of rev. back
==
git diff master~1:file.txt file.txt
(where branch is master and 1 revision back)


external diff tool like diffmerge
==
download diffmerge rpm, install it
in ~/.gitconfig
[difftool]
        prompt = false

run inside repo folder:
git difftool


git status in all repos
==
go to top level folder and run:
$ find . -type d -name '.git' | while read dir ; do sh -c "cd $dir/../ && echo -e \"\nGIT STATUS IN ${dir//\.git/}\" && $ git status -s && git branch" ; done


find text in versioned files
==
git grep YOUR_TEXT
(same as grep -R "WELCOMETONEDS" *)


delete / remove local branch
==
git branch -d the_local_branch


delete / remove remote branch
==
// first find it
git branch -a | grep YOUR_KEYWORD
// and delete
git push origin --delete YOUR_BRANCH


git log filter by user / author
==
git log --author="Jon"


show current revision
==
git rev-parse HEAD


update submodules
==
git submodule update
or
git submodule update --init --remote --recursive 


rename local branch
==
git branch -m <NEW_NAME>


skip verifications on commit (e.g. js lint)
==
--no-verify
git commit -m bla --no-verify


colors
==
~/.gitconfig:
[color]
  branch = auto
  diff = auto
  status = auto

[color "branch"]
  current = yellow reverse
  local = yellow
  remote = green

[color "diff"]
  meta = yellow bold
  frag = magenta bold
  old = red bold
  new = green bold

[color "status"]
  added = yellow
  changed = green
  untracked = cyan


pull “unable to resolve reference” “unable to update local ref”
==
git gc --prune=now


commit between dates
==
git log --after="2018-01-01" --until="2018-01-05"


search in log
==
git log --all --grep='Build 0051'


gitlab go vscode 
==
'go get' repository return error terminal prompts disabled
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
