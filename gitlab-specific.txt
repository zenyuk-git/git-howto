use private GitLab repo
==
generate GitLab access token, copy it
https://gitlab.com/-/profile/personal_access_tokens

create ~/.netrc file with permissions:
-rw-r--r--    .netrc

add to the .netrc file:

machine <YOUR-DOMAIN-NAME-FOR-PRIVATE-GITLAB, e.g. gitlab.zeny.uk>
login <YOUR-GITLAB-LOGIN, e.g. your email>
password <GITLAB-TOKEN e.g. glpat-abcd-abcdef-abcde>


